import pandas as pd
import spacy
from spacy.lang.fr.stop_words import STOP_WORDS as fr_stop

# load language model
nlp = spacy.load('fr_core_news_sm')

# Lread data from csv
df = pd.read_csv('memo_trees_extracts_1.csv')
text = df['extract'].tolist()
#10 samples to test with
samples = text[:10]

# remove 'nan' float objects, stopwords & lemmatize
lemma_words = []
for sample in samples:
    if isinstance(sample, str):
        sent = nlp(sample)
        # lemmatize
        for s in sent: 
            # lowercase
            s = s.lower()
            # remove stopwords
            if s in fr_stop:
                pass
            else:
                s = s.lemma_ 
                lemma_words.append(s)

# print(lemma_words)
# remove punctuation
punc = '''#$%&()*+, !"-— ■./.:;<=>?@[]^_`{|}...~»«—'''
for word in lemma_words:
    if word in punc or len(word) < 3:
        #print(word)
        lemma_words.remove(word)
#print(lemma_words)



