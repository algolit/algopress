import re

vowel_chars = r'aáàâäeêéèëoôóòöiïíìîuúùûüyÿAÁÀÂÄEÊÉÈËOÔÓÒÖIÏÍÌÎUÚÙÛÜYŸ'

vowels = r'[{}]'.format(vowel_chars)
consonants = r'[^{}\s\d]'.format(vowel_chars)

# Class to keep track of the words that have been removed from the text
# stores the removed words in an array 'pile'
class GarbageCollector (object):
    def __init__ (self):
        self.pile = []

    # Add garbage word to the pile
    def collect (self, garbage):
        self.pile.append(garbage)

    # Takes a match object and adds the matched text to the pile
    # returns an empty string.
    # Intended as a convenience function for use in re.sub
    def remove_and_collect_match (self, matched_garbage):
        if isinstance(matched_garbage, re.Match):
            self.pile.append(matched_garbage.group(0))
            return ''

        else:
            self.pile.append(matched_garbage)


garbage_collector = GarbageCollector()

# Remove words where there are six times more vowels than consonants or vice versa
def unbalanced_vowels_consonants (m):
    only_vowels = re.sub(consonants, '', m.group(0))
    vowel_count = len(only_vowels)
    consonent_count = len(m.group(0)) - vowel_count

    if vowel_count > consonent_count * 6 or consonent_count > vowel_count * 6:
        garbage_collector.collect(m.group(0))
        return ''
    else:
        return m.group(0)

# Remove words with more than 2 punctuation characters with the first and last character removed
def more_than_two_puntuation_chars (m):
    innertext = m.group(1)
    # Find all non-word characters + underscore, as the latter is part
    # of the word character class
    punctuation_chars = re.findall(r'[\W_]', innertext)

    # If the amount of punctuation chars is the same length
    # as the innertext, or has more than two punctuation marks
    # add word to garbage collector and return an emtpy string
    # to remove it from the text
    if len(punctuation_chars) >= min(len(innertext), 2):
        garbage_collector.collect(m.group(0))
        return ''
    
    return m.group(0)

# Remove words starting and ending with a lower case char, but with uppercase chars within it.
def has_unexpected_capitals (m):
  text = m.group(0)

  # Ensure first and last character are lowercase
  if text[0].islower() and text[-1].islower():
      # Loop through inner chars. As soon as one of them is a captial
      # add word to garbage collector and return an emtpy string
      # to remove it from the text
      for char in text[1:-1]:
          if char.isupper():
              garbage_collector.collect(text)
              return ''
          
  return text

with open('to_clean.txt') as h:
    text = h.read()
    # Remove words longer than 20 characters
    text = re.sub(r'[^\s\-]{20,}', garbage_collector.remove_and_collect_match, text)
    # Remove words with three identical characters after eachother
    text = re.sub(r'\S*(\S)\1{2,}\S*', garbage_collector.remove_and_collect_match, text, re.I)
    # Remove words longer than 2 characters consisting of only vowels or consonants
    text = re.sub(r'\b(' + vowels + r'{3,}|' + consonants + r'{3,})\b', garbage_collector.remove_and_collect_match, text, re.I)
    # Remove words where there are six times more vowels than consonants or vice versa
    text = re.sub(r'\b\S{6,}\b', unbalanced_vowels_consonants, text)
    # Remove words with more than 2 punctuation characters with the first and last character removed
    text = re.sub(r'\S(\S+)\S', more_than_two_puntuation_chars, text)
    # Remove words starting and ending with a lower case char, but with uppercase chars within it.
    text = re.sub(r'\w{3,}', has_unexpected_capitals, text)
    # Remove words which start, or end with 4 consonants
    text = re.sub(r'\b' + consonants + r'{4}\w*\b|\bw*' + consonants + r'{4}\b', garbage_collector.remove_and_collect_match, text)

    print('collected: ', garbage_collector.pile)

    print(text)