# Regex cleaner

A script which uses regular expressions to remove 'garbage' words from a text file (`to_clean.txt`).

The definition of garbage is based on a list of rules as defined in the paper: [Examining a Multi Layered Approach for Classification of OCR Quality without Ground Truth, Mirjam Cuper, 2022](https://journal.dhbenelux.org/journal/issues/004/article-3-Cuper.pdf)

> - A word with more than 20 characters is considered as garbage.
> - A word with more than 3 identical characters in a row is considered as garbage.
> - If a word has only vowels and consonants, and if the number of vowels is 8 times greater than the number of consonants or the other way around, the word is considered as garbage.
> - If we strip the first and last letter of a word, and there are more than two punctuation characters in the word, it is considered as garbage. 
> - If a word start and ends with a lower-case letter, and one of the remaining characters is upper-case, it is considered as garbage.
> 
> Furthermore, we added our own rules, based on Dutch language characteristics: 
> 
> ...
> 
> - If a word starts or ends with more than 4 consonants, it is considered as garbage.

Developed during the Algolit working session on regular expressions on March 22nd 2024.

Notes: <https://pad.constantvzw.org/p/algolit_240322>

License: Algolit 2024, [CC4r * COLLECTIVE CONDITIONS FOR RE-USE](https://constantvzw.org/wefts/cc4r.en.html)