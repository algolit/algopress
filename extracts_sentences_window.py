# script that extracts window of sentences around keyword in a sentence

import os
import pandas as pd
from nltk.tokenize import sent_tokenize

root_path='nature_articles/nature_arbre_tableau_autres_journaux_1830-1870'

extracts=[]
filenames=[]
word = 'arbre'

# open the folder with the files
for root, dirs, files in os.walk(root_path):
    print(root)
    for file in files:
        path= os.path.join(root, file)
        # open the file
        with open(path) as f:
            pos_sent = 0
            text = f.read()
            sentences = sent_tokenize(text, language='french')
            for sent in sentences:
                if word in sent:
                    # get position of the sentence
                    position = pos_sent
                    # create window of 3 sentences before and 3 sentences after
                    window = sentences[position-4] + sentences[position-3] + sentences[position-2] + sentences[position-1] + sentences[position] + sentences[position+1] + sentences[position+2] + sentences[position+3] + sentences[position+4]
                    # check for doubles and add if it doesn't exist yet
                    if window not in extracts:
                        extracts.append(window)
                        filenames.append(file)
            pos_sent += 1

# final check
print(len(extracts))
print(len(filenames))

# # save extracts in csv file
df=pd.DataFrame(list(zip(filenames, extracts)), columns=['filename', 'extract'])
df.to_csv('memo_trees_extracts_sentences.csv', index=False)
