# script that extracts window of words around keywords in a text

import os
import pandas as pd

root_path='/media/ana/My Passport/CAMille/BelgicaPress TXT/'


extracts=[]
filenames=[]

for root, dirs, files in os.walk(root_path):
    print(root)
    for file in files:
        path= os.path.join(root, file)
        with open(path) as f:
            text= f.read().lower()

            # loop through wordlist, check for position keywords arbre/s and add it to list pos_arbre
            text_words = text.split(' ')
            pos_arbre = []
            for i, w in enumerate(text_words):
                if w=='arbre' or w=='arbres':
                    #print(i, w)
                    pos_arbre.append(i)
            #print('positions arbres: ', pos_arbre)

            # create sublist of positions that are at less than 10 words distance from each other
            n = 0
            sublist = []
            list_of_lists = []
            while n < len(pos_arbre) - 1:
                if pos_arbre[n+1] - pos_arbre[n] < 100:
                    duo = pos_arbre[n], pos_arbre[n+1]
                    #print('duo:', duo)
                    pos_arbre[n] = duo
                    pos_arbre.remove(pos_arbre[n+1])
                n +=1
            #print(pos_arbre)

            # for each position or tuple of positions, take a window of 5 left and right
            for element in pos_arbre:
                if type(element)==tuple:
                    extract= ' '.join(text_words[element[0]-100:element[1]+100])
                    extracts.append(extract)
                    filenames.append(file)
                else:
                    extract=' '.join(text_words[element-100:element+100])
                    extracts.append(extract)
                    filenames.append(file)

# save extracts in csv file
df=pd.DataFrame(list(zip(filenames, extracts)), columns=['filename', 'extract'])
df.to_csv('memo_trees_extracts_1.csv', index=False)
